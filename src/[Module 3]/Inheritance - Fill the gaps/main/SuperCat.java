public class SuperCat extends Cat {

    public void clearLog() {
        /**
         * ( Super Heroes like SuperCat want to keep their secret actions private )
         */
        super.clearStringBuilder();
    }

}
