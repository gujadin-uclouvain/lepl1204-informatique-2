
public class NodeTraversal {

    public int val;

    public NodeTraversal left;
    public NodeTraversal right;

    public NodeTraversal(int val){
        this.val = val;
    }

    public boolean isLeaf(){
        return this.left == null && this.right == null;
    }

}
