import java.util.List;
import java.util.Stack; // this should give you a hint for the iterative version

public class Traversal {

    public static void recursiveInorder(NodeTraversal root, List<Integer> res) {
        if (root == null) {return;}
        if (root.left != null) {recursiveInorder(root.left, res);}
        res.add(root.val);
        if (root.right != null) {recursiveInorder(root.right, res);}
    }


    public static void iterativeInorder(NodeTraversal root, List<Integer> res){
        NodeTraversal currentNode = root;
        Stack<NodeTraversal> reminder = new Stack<>();

        while (currentNode != null || !reminder.empty()) {
            if (currentNode != null) {
                reminder.push(currentNode);
                currentNode = currentNode.left;
            } else {
                currentNode = reminder.pop();
                res.add(currentNode.val);
                currentNode = currentNode.right;
            }
        }
    }

}
