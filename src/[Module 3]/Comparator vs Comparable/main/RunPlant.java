import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class RunPlant {
    public static void main(String[] args) {
        Flower p1 = new Flower("poppy", 3, new Color(1,1,1));
        Flower p2 = new Flower("rose", 2, new Color(2,1,1));
        Flower p3 = new Flower("Poppy", 4, new Color(2,2,1));
        Flower p4 = new Flower("poppy", 2, new Color(2,2,1));
        Flower p5 = new Flower("poppy", 1, new Color(2,2,2));

        List<Flower> list = new ArrayList<Flower>();
        list.add(p3);
        list.add(p4);
        list.add(p2);
        list.add(p5);
        list.add(p1);

        List<Flower> sortedList = new ArrayList<Flower>();

        sortedList.add(p1);
        sortedList.add(p2);
        sortedList.add(p3);
        sortedList.add(p4);
        sortedList.add(p5);

        SorterPFT.sortFlowerByColor(list);

        System.out.println(list.equals(sortedList));
        for (int i = 0; i < sortedList.size(); i++) {
            System.out.println(list.get(i).toString());
        }
        System.out.println("-----");
        for (int i = 0; i < list.size(); i++) {
            System.out.println(sortedList.get(i).toString());
        }
    }
}
