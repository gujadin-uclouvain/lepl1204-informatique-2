import java.util.ArrayList;
import java.util.List;
import java.awt.Color;

public class SorterPFT {


    /*
     * Should sort the list and order the Flower by color, in this specific order : red, blue, green
     */
    public static void sortFlowerByColor(List<Flower> list){
        for (int i1 = 0; i1 < list.size() - 1; i1++) {
            for (int i2 = i1 + 1; i2 < list.size(); i2++) {
                Flower flowerA = list.get(i1);
                Flower flowerB = list.get(i2);
                int[] flowerAColor = new int[] {flowerA.getPetalColor().getRed(), flowerA.getPetalColor().getBlue(), flowerA.getPetalColor().getGreen()};
                int[] flowerBColor = new int[] {flowerB.getPetalColor().getRed(), flowerB.getPetalColor().getBlue(), flowerB.getPetalColor().getGreen()};

                if (flowerAColor[0] > flowerBColor[0]) {
                    list.set(i1, flowerB);
                    list.set(i2, flowerA);
                } else if (flowerAColor[0] == flowerBColor[0] && flowerAColor[1] > flowerBColor[1]) {
                    list.set(i1, flowerB);
                    list.set(i2, flowerA);
                } else if (flowerAColor[0] == flowerBColor[0] && flowerAColor[1] == flowerBColor[1] && flowerAColor[2] > flowerBColor[2]) {
                    list.set(i1, flowerB);
                    list.set(i2, flowerA);
                }
            }
        }
    }
    /*
     * Should sort the Plant by name only
     */
    public static void sortPlantByName(List<Plant> list){
        for (int i1 = 0; i1 < list.size() - 1; i1++) {
            for (int i2 = i1 + 1; i2 < list.size(); i2++) {
                String nameA = list.get(i1).getName();
                String nameB = list.get(i2).getName();
                int indexA = 0;
                int indexB = 0;
                boolean stop = false;
                while (!stop && ( indexA < nameA.length() || indexB < nameB.length() )) {
                    char currentCharA = nameA.charAt(indexA);
                    char currentCharB = nameB.charAt(indexB);
                    if (currentCharA > currentCharB) {
                        Plant tempA = list.get(i1);
                        list.set(i1, list.get(i2));
                        list.set(i2, tempA);
                        stop = true;
                    }
                    else if (currentCharA < currentCharB) {
                        stop = true;
                    }
                    indexA++;
                    indexB++;
                }
            }
        }
    }

    /*
     * Should sort the list and order the Tree by height
     */
    public static void sortTreeByHeight(List<Tree> list){
        for (int i1 = 0; i1 < list.size() - 1; i1++) {
            for (int i2 = i1 + 1; i2 < list.size(); i2++) {
                Tree treeA = list.get(i1);
                Tree treeB = list.get(i2);
                if (treeA.getHeight() > treeB.getHeight()) {
                    list.set(i1, treeB);
                    list.set(i2, treeA);
                }
            }
        }
    }

    public static void sortTreeByHeightUP(List<Tree> list){
        for (int i1 = 1; i1 < list.size(); i1++) {
            int i2 = i1 - 1;
            Tree key = list.get(i1);
            Tree var = list.get(i2);
            while (i2 >= 0 && var.getHeight() > key.getHeight()) {
                list.set(i2+1, var);
                list.set(i2, key);
                i2--; if (i2 >= 0) var = list.get(i2);
            }
        }
    }

    public static void main(String[] args) {
        Tree p1 = new Tree("poppy", 3, 10);
        Tree p2 = new Tree("rose", 2, 12);
        Tree p3 = new Tree("Poppy", 4, 9);
        Tree p4 = new Tree("poppy", 2, 11);

        List <Tree> list = new ArrayList<Tree>();

        list.add(p1);
        list.add(p2);
        list.add(p3);
        list.add(p4);

        sortTreeByHeightUP(list);
    }
}
