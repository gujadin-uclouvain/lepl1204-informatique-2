public class Plant implements Comparable<Plant>{

    private String name;
    private int age;

    public Plant(String name, int age){
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }


    /*
     * Should compare Plant by using name and age in that specific order
     * returns:
     *      > 0 if this is greater then o
     *      0 if they are equal
     *      < 0 if this is less than o
     */
    @Override
    public int compareTo(Plant o) {
        final String nameA = this.getName();
        final String nameB = o.getName();
        int indexA = 0;
        int indexB = 0;
        while (indexA < nameA.length() || indexB < nameB.length()) {
            char currentCharA = nameA.charAt(indexA);
            char currentCharB = nameB.charAt(indexB);
            if (currentCharA > currentCharB) {return 1;}
            else if (currentCharA < currentCharB) {return -1;}
            indexA++;
            indexB++;
        }
        if (this.getAge() > o.getAge()) {return 1;}
        else if (this.getAge() < o.getAge()) {return -1;}
        else {return 0;}
    }

    @Override
    public String toString() {
        return "name = " + this.getName() + " | " + "age = " + this.getAge();
    }
}
