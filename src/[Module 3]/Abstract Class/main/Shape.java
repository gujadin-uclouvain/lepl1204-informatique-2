public interface Shape {
    //Returns the area of the form.
    public double getArea(double d);

    //Returns the perimeter of the form.
    public double getPerimeter(double d);
}
