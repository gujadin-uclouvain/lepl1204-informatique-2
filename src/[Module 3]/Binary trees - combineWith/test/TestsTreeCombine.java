
import static org.junit.Assert.*;

import org.junit.*;
import org.junit.runner.RunWith;

import java.util.Random;
import java.util.stream.IntStream;

public class TestsTreeCombine {

    // for testing
    private static int MINIMAL_AMOUNT_OF_ELEMENTS_IN_TREE = 1;
    private static int MAXIMAL_AMOUNT_OF_ELEMENTS_IN_TREE = 17;
    private static int MINIMAL_VALUE = -15;
    private static int MAXIMAL_VALUE = 15;

    // to generate a random tree of n element(s)
    private int[] generateIntArray() {
        int size = new Random()
                .ints(1,
                        TestsTreeCombine.MINIMAL_AMOUNT_OF_ELEMENTS_IN_TREE,
                        TestsTreeCombine.MAXIMAL_AMOUNT_OF_ELEMENTS_IN_TREE
                )
                .sum();
        return new Random().ints(size, TestsTreeCombine.MINIMAL_VALUE, TestsTreeCombine.MAXIMAL_VALUE).toArray();
    }

    // to generate a tree given a int array
    private TreeCombine geneateRandomTree(int[] array) {
        NodeCombine root = null;
        root = insertLevelOrder(array, root, 0);
        return new TreeCombine(root);
    }

    // inspired by https://www.geeksforgeeks.org/construct-complete-binary-tree-given-array/
    private NodeCombine insertLevelOrder(int[] arr, NodeCombine root, int i) {
        // Base case for recursion
        if (i < arr.length) {
            root = new NodeCombine(arr[i]);

            // insert left child
            root.left = insertLevelOrder(arr, root.left,
                    2 * i + 1);

            // insert right child
            root.right = insertLevelOrder(arr, root.right,
                    2 * i + 2);
        }
        return root;
    }

    // Credits to https://www.quora.com/How-do-I-add-two-arrays-in-Java-and-initialize-the-third-array-with-the-sum-of-the-two-corresponding-elements-from-the-two-arrays/answer/Mar-Souf
    private int[] sum(int[] a, int[] b) {
        return IntStream
                .range(0, Math.max(a.length, b.length))
                .map(index -> (index < a.length ? a[index] : 0) + (index < b.length ? b[index] : 0))
                .toArray();
    }

    @Test
    public void test1() {
        TreeCombine t1 = new TreeCombine(
                new NodeCombine(
                        9,
                        new NodeCombine(6,
                                new NodeCombine(9),
                                new NodeCombine(2, new NodeCombine(4), null)
                        ),
                        new NodeCombine(14,
                                null,
                                new NodeCombine(11)
                        )
                )
        );

        TreeCombine t2 = new TreeCombine(
                new NodeCombine(
                        0,
                        new NodeCombine(-3, new NodeCombine(8), null),
                        new NodeCombine(8, new NodeCombine(5, null, new NodeCombine(1)), new NodeCombine(6))
                )
        );

        TreeCombine expected = new TreeCombine(
                new NodeCombine(
                        9,
                        new NodeCombine(3,
                                new NodeCombine(17),
                                new NodeCombine(2, new NodeCombine(4), null)
                        ),
                        new NodeCombine(22,
                                new NodeCombine(5, null, new NodeCombine(1)),
                                new NodeCombine(17)
                        )
                )
        );
        assertEquals(expected.root, t1.combineWith(t2).root);
    }

    @Test
    public void test2() {
        for (int i = 0; i < 100; i++) {
            int[] data1 = generateIntArray();
            int[] data2 = generateIntArray();
            int[] result = sum(data1, data2);
            TreeCombine t1 = geneateRandomTree(data1);
            TreeCombine t2 = geneateRandomTree(data2);
            TreeCombine expected = geneateRandomTree(result);
            assertEquals(expected.root, t1.combineWith(t2).root);
        }
    }

    @Test
    public void test_null_tree(){
        TreeCombine t2 = null;
        TreeCombine t3 = new TreeCombine(null);
        for (int i = 0; i < 100; i++) {
            int[] data1 = generateIntArray();
            TreeCombine t1 = geneateRandomTree(data1);
            assertEquals(t1.root, t1.combineWith(t2).root); // passed tree is null
            assertEquals(t1.root, t3.combineWith(t1).root); // root of actual tree is null
            assertEquals(t1.root, t1.combineWith(t3).root); // root of passed tree is null
        }


    }

}
