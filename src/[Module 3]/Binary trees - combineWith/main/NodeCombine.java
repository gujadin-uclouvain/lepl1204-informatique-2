public class NodeCombine {

    public int val;
    public NodeCombine left;
    public NodeCombine right;

    public NodeCombine(int val){
        this.val = val;
    }

    public NodeCombine(int val, NodeCombine left, NodeCombine right) {
        this.val = val;
        this.left = left;
        this.right = right;
    }

    public boolean isLeaf(){
        return this.left == null && this.right == null;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof NodeCombine)) return false;

        NodeCombine other = (NodeCombine) o;

        if (this.val != other.val) return false;
        if (this.isLeaf() != other.isLeaf()) return false;
        if (this.left != null && !this.left.equals(other.left)) return false;
        if (this.right != null && !this.right.equals(other.right)) return false;

        return true;
    }
}