public class TreeCombine {

    public NodeCombine root;

    public TreeCombine(NodeCombine root){
        this.root = root;
    }

    public TreeCombine combineWith(TreeCombine o) {
        if (root == null) {return o;}
        if (o == null || o.root == null) {return this;}
        return new TreeCombine(combineInRec(root, o.root));
    }

    private NodeCombine combineInRec(NodeCombine currentNodeA, NodeCombine currentNodeB) {
        if (currentNodeA == null) {return null;}

        if (currentNodeA.left != null && currentNodeB.left != null) {combineInRec(currentNodeA.left, currentNodeB.left);}
        if (currentNodeA.left == null && currentNodeB.left != null) {currentNodeA.left = currentNodeB.left;}

        currentNodeA.val += currentNodeB.val;

        if (currentNodeA.right != null && currentNodeB.right != null) {combineInRec(currentNodeA.right, currentNodeB.right);}
        if (currentNodeA.right == null && currentNodeB.right != null) {currentNodeA.right = currentNodeB.right;}

        return currentNodeA;
    }
}