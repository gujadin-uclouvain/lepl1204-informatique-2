public class TreeEquals {

    public NodeEquals root;

    public TreeEquals(NodeEquals root) {this.root = root;}

    @Override
    public boolean equals(Object o){
        if (o == null) {return false;}

        if (! o.getClass().equals(TreeEquals.class)) {return false;}

        if (root == null || ((TreeEquals)o).root == null) {
            return root == null && ((TreeEquals)o).root == null;
        }

        return root.equals(((TreeEquals)o).root);
    }
}