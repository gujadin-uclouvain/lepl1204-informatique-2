public class NodeEquals {

    public int val;
    public NodeEquals left;
    public NodeEquals right;

    public NodeEquals(int val) {this.val = val;}

    public boolean isLeaf() {return this.left == null && this.right == null;}

    @Override
    public boolean equals(Object o){
        boolean isEquals = (val == ((NodeEquals)o).val);

        if (left != null && ((NodeEquals)o).left != null) {
            isEquals = (isEquals && left.equals( ((NodeEquals)o).left ));
        }

        if (right != null && ((NodeEquals)o).right != null) {
            isEquals = (isEquals && right.equals( ((NodeEquals)o).right) );
        }

        return isEquals;
    }
}