
import org.junit.Test;
import java.util.Stack;
import java.util.function.Supplier;
import java.util.stream.Stream;

import static org.junit.Assert.*;

public class TestsEqualsTree {

    Supplier<Integer> rnd = () -> (int) (Math.random() * 100);

    @Test
    public void randomTreeTest(){

        for(int i = 0; i < 10; i++) {

            Integer[] seeds = Stream.generate(rnd).limit(15).toArray(Integer[]::new);
            NodeEquals root = new NodeEquals(0);
            NodeEquals root2 = new NodeEquals(0);
            root = buildTree(root, seeds, 0);
            root2 = buildTree(root2, seeds, 0);
            TreeEquals tree = new TreeEquals(root);
            TreeEquals tree2 = new TreeEquals(root2);

            assertEquals(tree, tree2); // root.equals(root2) == true
        }
    }


    @Test
    public void simpleTreeTest(){

        Integer [] seeds = Stream.generate(rnd).limit(7).toArray(Integer[]::new);

        NodeEquals root = new NodeEquals(seeds[0]);
        root.left = new NodeEquals(seeds[1]);
        root.right = new NodeEquals(seeds[2]);
        root.left.left = new NodeEquals(seeds[3]);
        root.left.right = new NodeEquals(seeds[4]);
        root.right.left = new NodeEquals(seeds[5]);
        root.right.right = new NodeEquals(seeds[6]);

        TreeEquals tree = new TreeEquals(root);

        NodeEquals root2 = new NodeEquals(seeds[0]);
        root2.left = new NodeEquals(seeds[1]);
        root2.right = new NodeEquals(seeds[2]);
        root2.left.left = new NodeEquals(seeds[3]);
        root2.left.right = new NodeEquals(seeds[4]);
        root2.right.left = new NodeEquals(seeds[5]);
        root2.right.right = new NodeEquals(seeds[5]);

        TreeEquals tree2 = new TreeEquals(root2);

        assertNotEquals(tree, tree2);

        root2.right.right = new NodeEquals(seeds[6]);

        tree2 = new TreeEquals(root2);

        assertEquals(tree, tree2);
    }


    @Test
    public void cornerCasesTest(){

        TreeEquals tree = new TreeEquals(null);
        TreeEquals tree2 = new TreeEquals(null);

        assertEquals(tree, tree2);

        tree2 = new TreeEquals(new NodeEquals(1));

        assertNotEquals(tree, tree2);
        assertNotEquals(tree, null);
        assertNotEquals(tree, new Stack<>());

    }




    private NodeEquals buildTree(NodeEquals root, Integer [] seeds, int idx){

        if(idx < seeds.length){

            root = new NodeEquals(seeds[idx]);

            root.left = buildTree(root.left, seeds, 2*idx+1);
            root.right = buildTree(root.right, seeds, 2*idx+2);
        }

        return root;
    }
}