import java.util.Arrays;

public class Valley{
    /*
     * Example:
     * [1, 1, 1, -1, -1, -1, -1, -1, 1, 1, 1, 1, 1, 1, -1, -1]
     * Should return
     * [5, 3]
     */

    public static int[] valley (int[] array){
        int[] inverse = new int[array.length];
        for (int i = 0; i < array.length; i++) {inverse[i] = -array[i];}
        return new int[]{findValley(array), findValley(inverse)};
    }

    public static int findValley(int[] array) {
        int[] down = new int[array.length+1];
        int[] up = new int[array.length+1];

        for(int i = 0; i < array.length; i++) {
            if(array[i] <= 0) {
                down[i+1] = down[i] - array[i];
            }
        }

        for(int i = array.length - 1; i >= 0; i--) {
            if(array[i] >= 0) {
                up[i] = up[i + 1] + array[i];
            }
        }

        int highestValley = 0;
        for(int i = 0; i < array.length; i++) {
            int v = Math.min(down[i], up[i]);
            if(v > highestValley)
                highestValley = v;
        }
        return  highestValley;
    }

    public static void main(String[] args) {
        System.out.println(Arrays.toString(valley(new int[]{1, 1, 1, -1, -1, -1, -1, -1, 1, 1, 1, 1, 1, 1, -1, -1})));
    }
}
