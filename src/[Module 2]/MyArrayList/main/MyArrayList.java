import java.util.ConcurrentModificationException;
import java.util.Iterator;

public class MyArrayList<Item> implements Iterable<Item> {

    private Object [] list;
    private int size;

    public MyArrayList(int initSize){
        if (initSize < 0) throw new java.lang.IndexOutOfBoundsException();
        list = new Object[initSize];
        size = 0;
    }

    /*
    * Checks if 'list' needs to be resized then add the element at the end 
    * of the list.
    */
    public void enqueue(Item item){
        if (size == list.length) {
            Object[] newList = new Object[list.length * 2];
            for (int index = 0; index < list.length; index++) {
                newList[index] = list[index];
            }
            list = newList;
        }
        list[size] = item;
        size++;
    }

    /*
    * Removes the element at the specified position in this list.
    * Returns the element that was removed from the list. You need to
    * resize when removing an element.
    */
    public Item remove(int index) {
        if (index < 0 || index > size - 1) throw new java.lang.IndexOutOfBoundsException();
        else if (index == size) {
            Object removedElem = list[index];
            list[index] = null;
            size--;
            return (Item) removedElem;
        } else {
            Object removedElem = list[index];
            int moveIndex = index;
            while (moveIndex < size - 1) {
                list[moveIndex] = list[moveIndex + 1];
                moveIndex++;
            }
            list[moveIndex] = null;
            size--;
            return (Item) removedElem;
        }
    }

    public int size(){
        return this.size;
    }

    public String toString(){
        return java.util.Arrays.toString(list);
    }

    @Override
    public Iterator<Item> iterator() {
        return new MyArrayListIterator();
    }

    private class MyArrayListIterator implements Iterator<Item> {

        private int counter = 0;
        private final int nInit = size;

        private boolean failFastCheck() {
            if (nInit != size()) throw new ConcurrentModificationException("An element has been added or removed during the iteration on the Array.");
            return true;
        }

        @Override
        public boolean hasNext() {
            return failFastCheck() && (counter != size);
        }

        @Override
        public Item next() {
            failFastCheck();
            if (!hasNext()) throw new java.util.NoSuchElementException("This is the Array's end.");
            return (Item) list[counter++];
        }
    }
}