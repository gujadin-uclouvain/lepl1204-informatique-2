public class RunMyArrayList {
    public static void main(String[] args) {
        MyArrayList<Integer> list = new MyArrayList<>(5);
        list.enqueue(0);
        list.enqueue(1);
        list.enqueue(2);
        list.enqueue(3);
        list.enqueue(4);
        list.enqueue(5);
        System.out.println(list.remove(3));
        System.out.println(list.remove(3));
        System.out.println(list.remove(3));

        System.out.println(list.toString());
    }
}
