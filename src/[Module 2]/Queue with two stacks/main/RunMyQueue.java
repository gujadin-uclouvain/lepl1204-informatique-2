public class RunMyQueue {
    public static void main(String[] args) {
        MyQueue<Integer> queue = new MyQueue<>();
        System.out.println(queue.empty());
        for (int i = 0; i <= 10; i++) {
            queue.enqueue(i);
        }
        for (int i = 0; i <= 20; i++) {
            System.out.println("-------");
            System.out.println(queue.peek());
            System.out.println(queue.dequeue());
            queue.enqueue(i+11);
            System.out.println(queue.empty());
        }
    }
}
