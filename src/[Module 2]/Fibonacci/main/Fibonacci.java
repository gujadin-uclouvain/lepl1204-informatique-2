public class Fibonacci {


    public static int fiboRecursive(int index){
        if (index <= 1) {return index;}
        return fiboRecursive(index - 1) + fiboRecursive(index - 2);
    }


    public static int fiboIterative(int index){
        if (index <= 1) {return index;}
        int f0 = 0;
        int f1 = 1;
        for (int i = 2; i <= index; i++) {
            int f2 = f0 + f1;
            f0 = f1;
            f1 = f2;
        }
        return f1;
    }

    public static void main(String[] args) {
        System.out.println(fiboIterative(10));
        System.out.println(fiboRecursive(10));
    }

}
