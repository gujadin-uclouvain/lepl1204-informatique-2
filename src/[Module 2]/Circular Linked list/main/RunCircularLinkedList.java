public class RunCircularLinkedList {
    public static void main(String[] args) {
        CircularLinkedList<Integer> list = new CircularLinkedList<>();
        list.enqueue(0);
        list.enqueue(1);
        list.enqueue(2);
        list.enqueue(3);
        list.enqueue(4);
        System.out.println(list.remove(0));
        System.out.println(list.remove(0));
        System.out.println(list.remove(list.size()-1));
        list.enqueue(5);
    }
}
