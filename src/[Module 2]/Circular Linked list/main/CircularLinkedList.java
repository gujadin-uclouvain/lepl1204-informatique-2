import java.util.ConcurrentModificationException;
import java.util.Iterator;

public class CircularLinkedList<Item> implements Iterable<Item> {

    private int n;          // size of the list
    private Node last;   // trailer of the list

    // helper linked list class
    private class Node {

        private Item item;
        private Node next;

        private Node(Item item){
            this.item = item;
            this.next = null;
        }

    }

    public CircularLinkedList() {
        last = null;
        n = 0;
    }

    public boolean isEmpty() {return n == 0;}

    public int size() {return n;}

    public Node getLast() {return last;}

    public Item getItem(Node n) {return n.item;}


    /**
     * Append an item at the end of the list
     * @param item the item to append
     */
    public void enqueue(Item item) {
        if (isEmpty()) {
            last = new Node(item);
            last.next = last;
        } else {
            Node newNode = new Node(item);
            Node first = last.next;
            last.next = newNode;
            last = newNode;
            last.next = first;
        }
        n++;
    }

    /**
     * Removes the element at the specified position in this list.
     * Shifts any subsequent elements to the left (subtracts one from their indices).
     * Returns the element that was removed from the list.
    */
    public Item remove(int index) {
        if (isEmpty()) throw new java.util.NoSuchElementException("You cannot remove an element in an empty Linked List.");
        if (index < 0 || index >= size()) throw new IndexOutOfBoundsException("You need to choose an index between 0 and " + (size() - 1) + ".");

        if (size() == 1) {
            Item value = last.item;
            last = null;
            n = 0;
            return value;
        }

        Node current = last.next;
        Node beforeCurrent = last;
        for (int finder = 0; finder < index; finder++) {
            current = current.next;
            beforeCurrent = beforeCurrent.next;
        }

        Item value = current.item;
        beforeCurrent.next = current.next;
        if (current.equals(last)) {
            last = beforeCurrent;
        }
        n--;
        return value;
    }


    /**
     * Returns an iterator that iterates through the items in FIFO order.
     * @return an iterator that iterates through the items in FIFO order.
    */
    public Iterator<Item> iterator() {
        return new ListIterator();
    }

    /**
     * Implementation of an iterator that iterates through the items in FIFO order.
     *
    */
    private class ListIterator implements Iterator<Item> {

        private Node current;
        private final int nInit = size();
        private int counter = 0;

        private ListIterator() {
            if (last == null) {current = null;}
            else {current = last.next;}
        }

        private boolean failFastCheck() {
            if (nInit != size()) throw new ConcurrentModificationException("An element has been added or removed during the iteration on the Linked List.");
            return true;
        }

        @Override
        public boolean hasNext() {
            return failFastCheck() && current != null && counter != size();
        }

        @Override
        public Item next() {
            failFastCheck();
            if (!hasNext()) throw new java.util.NoSuchElementException("You cannot iterate in an empty Linked List. / You iterate on the entire Linked List.");
            Item value = current.item;
            current = current.next;
            counter++;
            return value;
        }
    }
}
