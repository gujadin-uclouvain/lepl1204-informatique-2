import java.util.Arrays;

public class SumSubarray {
    /*
    *    Given the array [-2,1,-3,4,-1,2,1,-5,4]
    *    The contiguous subarray that produces the best result is [4,-1,2,1]
    *    For this array your method should return [6, 3, 6]
    */
    public static int[] maxSubArray(int[] a){
        int [] subOptimal = new int[]{Integer.MIN_VALUE, 0, 0};
        int currentMax = 0;
        int currentFinalIndex = 0;
        int startIndex = 0;
        while (currentFinalIndex < a.length) {
            currentMax += a[currentFinalIndex];
            if (currentMax > subOptimal[0]) {
                subOptimal[0] = currentMax;
                subOptimal[1] = startIndex;
                subOptimal[2] = currentFinalIndex;
            }
            if (currentMax < 0) {
                currentMax = 0;
                startIndex = currentFinalIndex + 1;
            }
            currentFinalIndex++;
        }
        return subOptimal;
    }

    public static int[] maxSubArrayWorst(int[] a) {
        int[] maxArray = new int[]{Integer.MIN_VALUE, 0, 0};
        for (int i = 0; i < a.length; i++){
            int current = 0;
            for (int j = i; j < a.length; j++){
                current += a[j];
                if (current > maxArray[0]){
                    maxArray[0] = current;
                    maxArray[1] = i;
                    maxArray[2] = j;
                }
            }
        }
        return maxArray;
    }

    public static void main(String[] args) {
        System.out.println(Arrays.toString(maxSubArray(new int[]{-2, 1, -3, 4, -1, 2, 1, -5, 4})));
    }
}
