import java.util.Stack;

public class TowerOfHanoi{

    /*
     * we want the tower to be moved from a to b.
     * The first value of n is the size of the stack.
     */
    public static void towerOfHanoi(int n, Stack<Disk> a, Stack<Disk> b, Stack<Disk> c) {
        if (n == 0) {return;}
        towerOfHanoi(n-1, a, c, b);
        b.push(a.pop());
        towerOfHanoi(n-1, c, b, a);
    }

    public static int numberOfMoves(int stackSize){
        return (int) Math.pow(2, stackSize) - 1;
    }

    public static void main(String[] args) {
        System.out.println(numberOfMoves(4));
    }
}
