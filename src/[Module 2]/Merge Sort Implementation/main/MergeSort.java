public class MergeSort {

    /**
     * Merge the array from lo to hi
     */
    private static void merge(int[] a, int[] aux, int lo, int mid, int hi) {
        for (int index = lo; index <= hi; index++) {
            aux[index] = a[index];
        }

        int loRef = lo;
        int midRef = mid + 1;
        for (int index = lo; index <= hi; index++) {
            if (loRef > mid) {
                a[index] = aux[midRef++];
            } else if (midRef > hi) {
                a[index] = aux[loRef++];
            } else if (aux[loRef] < aux[midRef]) {
                a[index] = aux[loRef++];
            } else {
                a[index] = aux[midRef++];
            }
        }
    }
    /**
     *  Split the array and call merge
     */
    public static void sort(int[] a) { sortRecursive(a, new int[a.length], 0, a.length-1); }

    private static void sortRecursive(int[] a, int[] aux, int lo, int hi) {
        if (lo == hi){
            return;
        }
        int mid = lo + (hi-lo)/2;
        sortRecursive(a, aux, lo, mid);
        sortRecursive(a, aux, mid+1, hi);
        merge(a, aux, lo, mid, hi);
    }

    public static void main(String[] args) {
        sort(new int[]{38, 27, 43, 3, 9, 82, 10});
    }


}
