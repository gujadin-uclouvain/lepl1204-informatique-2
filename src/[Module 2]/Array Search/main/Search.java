public class Search {
    /**
     *
     * @param tab
     * @return
     */
    public static int searchNaif(int[] tab, int elem){
        int index = 0;
        while (index < tab.length){
            if (tab[index] == elem){
                return index;
            }
            index++;
        }
        return -1;
    }

    public static int binarySearch(int[] tab, int elem){
        int before = 0; int after = tab.length-1;
        while (before <= after) {
            int middle = before + (after - before)/2;
            if (tab[middle] > elem) after = middle - 1; //Before Middle
            else if (tab[middle] < elem) before = middle + 1; // After Middle
            else return middle;
        }
        return -1;
    }

    public static void main(String[] args) {
        System.out.println(
                binarySearch(new int[]{0, 2, 4, 6, 8, 10, 12, 14, 16, 18}, 6)
        );
    }
}
