public class LauncherThread {

    /*
     * Instantiate and start each thread from "t" with its OWN Counter object,
     * then wait for all threads to finish and return the set of Counter objects
     * the threads ran on. Each thread must be named according to its index in the
     * "t" array.
     */
    public static CounterThread[] init(Thread[] t){
        CounterThread[] result = new CounterThread[t.length];
        for (int index = 0; index < t.length; index++) {
          CounterThread counter = new CounterThread();
          t[index] = new Thread(counter);
          t[index].setName(Integer.toString(index));
          t[index].start();
          result[index] = counter;
        }

        for (Thread thread : t) {
          try {
              thread.join();
          } catch (InterruptedException e) {
              throw new RuntimeException("Unexpected interrupt", e);
          }
        }
        return result;
    }
}
