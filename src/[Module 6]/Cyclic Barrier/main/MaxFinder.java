import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

public class MaxFinder {

    private final int nThreads,length,width,depth;
    private final int[][][] data;
    private final CyclicBarrier barrier;
    private int[] sums;
    private int max;

    /*
     * Worker constructor takes only one parameter int r, which is his associated row number
     * A worker is responsible of the calculation of the sum of each 2D-Array with row == r + nThread * round; with round >= 0
     *
     * Run should compute the sum of a 2D-array and store the result in sums[] then wait for the cyclic barrier to get the result
     * And restart computing nThreads further
     */
    class Worker implements Runnable {
        private int row;
        public Worker(int r) { this.row = r; }

        @Override
        public void run() {
            while (row < length) {
                int sum = 0;
                for (int i = 0; i < width; i++) {
                    for (int j = 0; j < depth; j++) {
                        sum += data[row][i][j];
                    }
                }
                sums[row % nThreads] = sum;
                row += nThreads;
                try {
                    barrier.await();
                } catch (InterruptedException | BrokenBarrierException e) {
                    return;
                }
            }
        }
    }


    /*
     *
     * Initialize all the instance variable and start the right amount of Threads
     *
     */
    private MaxFinder(int[][][] matrix, int nThreads) throws InterruptedException{
        this.data = matrix;
        this.length = matrix.length;
        this.width = matrix[0].length;
        this.depth = matrix[0][0].length;
        this.nThreads = nThreads;

        sums = new int[nThreads];
        barrier = new CyclicBarrier(nThreads, () -> {
            for (int index = 0; index < nThreads; index++) {
                max = Math.max(max, sums[index]);
            }
            sums = new int[nThreads];
        });

        List<Thread> threads = new ArrayList<>(nThreads);
        for (int indexThread = 0; indexThread < nThreads; indexThread++) {
            Thread thread = new Thread(new Worker(indexThread));
            threads.add(thread);
            thread.start();
        }

        // wait until done
        for (Thread thread : threads)
            thread.join();
    }

    /*
    * subSize is the length of the subarray
    * rowSize is the rowlength for all the array
    *
    */
    public static int getMaxSum(int[][][] matrix, int nThreads){
        try {
            MaxFinder mf = new MaxFinder(matrix, nThreads);
            return mf.max;
        } catch (InterruptedException e) {
            return -1;
        }
    }

     public static int[][][] rand3DMatrix(int x, int y, int z){
        Random gen = new Random(); int[][][] output = new int[x][y][z];
        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {
                for (int k = 0; k < z; k++) {
                    output[i][j][k] = gen.nextInt(9);
                }
            }
        }
        return output;
    }

    public static void main(String[] args) {
        int[][][] test = rand3DMatrix(4, 2, 2);
        System.out.println(Arrays.deepToString(test));
        System.out.println(getMaxSum(test, 2));
    }
}
