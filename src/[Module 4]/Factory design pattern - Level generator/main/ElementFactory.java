public class ElementFactory extends Factory {

    private static ElementFactory currentFactory = null;
    private ElementFactory() {}

    public static ElementFactory getInstance() {
        if (currentFactory == null) {
            currentFactory = new ElementFactory();
        }
        return currentFactory;
    }

    @Override
    public LevelComponent getElement(char c) {
        if (c == 'D') { return new Door(); }
        else if (c == 'K') { return new Key(); }
        else if (c == '#') { return new Wall(); }
        else if (c == '-') { return new Floor(); }
        throw new IllegalArgumentException("This character represents nothing existing");
    }
}
