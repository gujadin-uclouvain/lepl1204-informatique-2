public class Level extends AbstractLevel {

    private String str;

    public Level(String input){
        this.str = input;
        String[] arrayStr = input.split("\n");
        int lineSize = arrayStr[0].length();
        super.size = lineSize*lineSize;
        super.components = new LevelComponent[lineSize][lineSize];
        componentParser(arrayStr);
    }

    private void componentParser(String[] arrayStr){
        for (int indexLine = 0; indexLine < arrayStr.length; indexLine++) {
            for (int indexColumn = 0; indexColumn < arrayStr.length; indexColumn++) {
                char elem = arrayStr[indexLine].charAt(indexColumn);
                components[indexLine][indexColumn] = this.getElement(elem);
            }
        }
    }

    public LevelComponent getElement(char  c){
        return ElementFactory.getInstance().getElement(c);
    }

    @Override
    public String toString() {
        return str;
    }

    @Override
    LevelComponent[][] getComponents() {
        return components;
    }

    @Override
    int getSize() {
        return size;
    }
}
