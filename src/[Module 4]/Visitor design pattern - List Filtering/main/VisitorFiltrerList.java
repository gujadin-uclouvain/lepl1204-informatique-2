import java.util.List;

public class VisitorFiltrerList extends VisitorFiltrer {

    public VisitorFiltrerList(Class cls) {
        super(cls);
    }

    @Override
    List<Object> getFiltered() {
        return super.filtered;
    }

    @Override
    void visit(VisitableFiltrer visitableFiltrer) {
        for (Object elem : visitableFiltrer.elements) { this.visit(elem); }
    }

    @Override
    void visit(Object o) { if (super.toFilter == o.getClass()) { super.filtered.add(o); }
    }
}
