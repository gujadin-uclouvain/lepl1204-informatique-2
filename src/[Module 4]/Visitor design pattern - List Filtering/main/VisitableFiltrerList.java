public class VisitableFiltrerList extends VisitableFiltrer {

    public VisitableFiltrerList(Object[] objectsList) {
        super.elements = objectsList;
    }

    @Override
    void accept(VisitorFiltrer visitorFiltrer) {
        for (Object element : this.elements) {
            visitorFiltrer.visit(element);
        }
    }
}
