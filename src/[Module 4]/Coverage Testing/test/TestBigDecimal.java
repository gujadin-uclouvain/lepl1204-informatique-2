import org.junit.Test;
import static org.junit.Assert.*;

public class TestBigDecimal{

    @Test
    public void testBasic1(){
         String str = "0000000004242";
         char[] in = str.toCharArray();
         int offset = 0;
         int len = in.length;

         assertEquals(4242, BigDecimal.parseExp(in, offset, len) );
    }

    @Test
    public void testBasic2(){
         String str = "0000000004242";
         char[] in = str.toCharArray();
         int offset = 0;
         int len = 11;

         assertEquals(42, BigDecimal.parseExp(in, offset, len) );
    }

    @Test
    public void testBasic3(){
         String str = "0000000004242";
         char[] in = str.toCharArray();
         int offset = 0;
         int len = 4;

         assertEquals(0, BigDecimal.parseExp(in, offset, len) );
    }

    @Test
    public void testPositive1(){
         String str = "++768549321";
         char[] in = str.toCharArray();
         int offset = 0;
         int len = in.length;

         assertEquals(768549321, BigDecimal.parseExp(in, offset, len) );
    }

    @Test
    public void testPositive2(){
         String str = "++768549321";
         char[] in = str.toCharArray();
         int offset = 0;
         int len = 6;

         assertEquals(7685, BigDecimal.parseExp(in, offset, len) );
    }

    @Test
    public void testPositive3(){
         String str = "+768549321";
         char[] in = str.toCharArray();
         int offset = 0;
         int len = 6;

         assertEquals(76854, BigDecimal.parseExp(in, offset, len) );
    }

    @Test
    public void testNegative1(){
         String str = "--768549321";
         char[] in = str.toCharArray();
         int offset = 0;
         int len = 6;

         assertEquals(-7685, BigDecimal.parseExp(in, offset, len) );
    }

    @Test
    public void testNegative2(){
         String str = "--768549321";
         char[] in = str.toCharArray();
         int offset = 0;
         int len = 6;

         assertEquals(-7685, BigDecimal.parseExp(in, offset, len) );
    }

    @Test
    public void testNegative3(){
         String str = "-768549321";
         char[] in = str.toCharArray();
         int offset = 0;
         int len = 6;

         assertEquals(76854, BigDecimal.parseExp(in, offset, len) );
    }

    @Test (expected = NumberFormatException.class)
    public void testSmallLenError(){
         String str = "--768549321";
         char[] in = str.toCharArray();
         int offset = 0;
         int len = 42;

         BigDecimal.parseExp(in, offset, len);
    }

    @Test (expected = NumberFormatException.class)
    public void testBigLenError(){
         String str = "--768549321";
         char[] in = str.toCharArray();
         int offset = 0;
         int len = -1;

         BigDecimal.parseExp(in, offset, len);
    }

    @Test (expected = NumberFormatException.class)
    public void testLetters(){
         String str = "InGiNiOuS";
         char[] in = str.toCharArray();
         int offset = 0;
         int len = in.length;

         BigDecimal.parseExp(in, offset, len);
    }

}