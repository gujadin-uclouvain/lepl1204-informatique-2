public class Client extends Observer {

    public Client(int zone) {
        super.zone = zone;
        super.news = "";
    }

    @Override
    public void update(Object o) {
        news = o.toString();
    }

    @Override
    public int getZone() {
        return zone;
    }

    @Override
    public String getNews() {
        return news;
    }
}
