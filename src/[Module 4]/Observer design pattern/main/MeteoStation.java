public class MeteoStation extends Observable {

    public MeteoStation() {}

    @Override
    public Observer[] getSubscribers() {
        Observer[] subResult = new Observer[subscribers.size()];
        for (int i = 0; i < subscribers.size() - 1; i++) {
            subResult[i] = subscribers.get(i);
        }
        return subResult;
    }

    @Override
    public void broadcast(Pair<String, Integer> pair) {
        for (Observer subscriber : subscribers) {
            if (subscriber.getZone() == pair.getZone()) { subscriber.update(pair.getAlert()); }
        }
    }

    @Override
    public void addObserver(Observer sub) {
        boolean isSame = false;
        for (Observer subscriber : subscribers) {
            if (subscriber.equals(sub)) { isSame = true; break;}
        }
        if (!isSame) { subscribers.add(sub); }
    }

    @Override
    public boolean removeObserver(Observer sub) {
        return subscribers.remove(sub);
    }

    @Override
    public void setAlert(String alert, int zone) {
        Pair<String, Integer> newPair = new Pair<>(alert, zone);
        int index = 0;
        boolean isFind = false;
        while (index < zones.size() && !isFind){
            if (newPair.equals(zones.get(index))) { zones.set(index, newPair); isFind = true; }
            index++;
        }
        if (!isFind) {zones.add(newPair);}
        broadcast(newPair);
    }
}
