public interface VisitorCalculator {

    public int visit(AddCalculator visitable);

    public int visit(MultCalculator visitable);

    public int visit(DivCalculator visitable);

    public int visit(SubCalculator visitable);

    public int visit(LeafCalculator visitable);
}
