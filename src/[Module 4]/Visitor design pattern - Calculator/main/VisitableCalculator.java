public interface VisitableCalculator {

    public int accept(VisitorCalculator visitorCalculator);

}
