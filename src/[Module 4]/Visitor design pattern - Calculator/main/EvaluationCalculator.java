public class EvaluationCalculator implements VisitorCalculator {

    private int leftCasting(NodeCalculator visitable) {
        if (visitable.getLeft().getClass() == AddCalculator.class) { return this.visit((AddCalculator) visitable.getLeft()); }
        else if (visitable.getLeft().getClass() == SubCalculator.class) { return this.visit((SubCalculator) visitable.getLeft()); }
        else if (visitable.getLeft().getClass() == MultCalculator.class) { return this.visit((MultCalculator) visitable.getLeft()); }
        else if (visitable.getLeft().getClass() == DivCalculator.class) { return this.visit((DivCalculator) visitable.getLeft()); }
        else if (visitable.getLeft().getClass() == LeafCalculator.class) { return this.visit((LeafCalculator) visitable.getLeft()); }
        else throw new IllegalArgumentException();
    }

    private int rightCasting(NodeCalculator visitable) {
        if (visitable.getRight().getClass() == AddCalculator.class) { return this.visit((AddCalculator) visitable.getRight()); }
        else if (visitable.getRight().getClass() == SubCalculator.class) { return this.visit((SubCalculator) visitable.getRight()); }
        else if (visitable.getRight().getClass() == MultCalculator.class) { return this.visit((MultCalculator) visitable.getRight()); }
        else if (visitable.getRight().getClass() == DivCalculator.class) { return this.visit((DivCalculator) visitable.getRight()); }
        else if (visitable.getRight().getClass() == LeafCalculator.class) { return this.visit((LeafCalculator) visitable.getRight()); }
        else throw new IllegalArgumentException();
    }

    @Override
    public int visit(AddCalculator visitable) {
        return leftCasting(visitable) + rightCasting(visitable);
    }

    @Override
    public int visit(MultCalculator visitable) {
        return leftCasting(visitable) * rightCasting(visitable);
    }

    @Override
    public int visit(DivCalculator visitable) {
        int denominator = rightCasting(visitable);
        if (denominator == 0) throw new IllegalArgumentException();
        return leftCasting(visitable) / denominator;
    }

    @Override
    public int visit(SubCalculator visitable) {
        return leftCasting(visitable) - rightCasting(visitable);
    }

    @Override
    public int visit(LeafCalculator visitable) {
        return visitable.getValue();
    }
}
