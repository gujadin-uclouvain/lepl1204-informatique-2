public class LeafCalculator implements VisitableCalculator {

    private Integer value;

    public LeafCalculator(Integer integer) {
        this.value = integer;
    }

    public int getValue() {
        return value;
    }

    @Override
    public int accept(VisitorCalculator visitorCalculator) {
        return visitorCalculator.visit(this);
    }
}
