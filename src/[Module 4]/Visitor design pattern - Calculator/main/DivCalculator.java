public class DivCalculator extends NodeCalculator implements VisitableCalculator {

    public DivCalculator(VisitableCalculator left, VisitableCalculator right) {
        super(left, right);
    }

    @Override
    public int accept(VisitorCalculator visitorCalculator) {
        return visitorCalculator.visit(this);
    }
}
