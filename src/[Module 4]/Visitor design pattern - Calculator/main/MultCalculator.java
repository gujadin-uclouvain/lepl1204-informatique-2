public class MultCalculator extends NodeCalculator implements VisitableCalculator {

    public MultCalculator(VisitableCalculator left, VisitableCalculator right) {
        super(left, right);
    }

    @Override
    public int accept(VisitorCalculator visitorCalculator) {
        return visitorCalculator.visit(this);
    }
}
