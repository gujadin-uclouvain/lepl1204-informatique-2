abstract class NodeCalculator {

    private VisitableCalculator left;
    private VisitableCalculator right;

    public NodeCalculator(VisitableCalculator left, VisitableCalculator right){
        this.left = left;
        this.right = right;
    }

    public VisitableCalculator getLeft() { return left; }

    public VisitableCalculator getRight() { return right; }

}
