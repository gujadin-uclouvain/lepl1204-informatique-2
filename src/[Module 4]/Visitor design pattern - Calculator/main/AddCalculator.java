public class AddCalculator extends NodeCalculator implements VisitableCalculator {

    public AddCalculator(VisitableCalculator left, VisitableCalculator right) {
        super(left, right);
    }

    @Override
    public int accept(VisitorCalculator visitorCalculator) {
        return visitorCalculator.visit(this);
    }
}
