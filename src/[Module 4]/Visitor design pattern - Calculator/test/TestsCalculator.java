import com.github.guillaumederval.javagrading.Grade;
import com.github.guillaumederval.javagrading.GradeFeedback;
import com.github.guillaumederval.javagrading.GradingRunner;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.function.Supplier;
import java.util.stream.Stream;

import static org.junit.Assert.*;

@RunWith(GradingRunner.class)
public class TestsCalculator {

    private Supplier<Integer> rng = () -> (int) ((Math.random()*100) + 1); // never generate 0

    @Test
    @Grade
    @GradeFeedback(message = "Your code couldn't handle a simple expression of type (a operand b)", onFail = true)
    public void testBasicTrees(){

        VisitorCalculator calculator = new EvaluationCalculator();
        Integer [] seeds = Stream.generate(rng).limit(2).toArray(Integer[]::new);

        NodeCalculator root = new AddCalculator(new LeafCalculator(seeds[0]), new LeafCalculator(seeds[1]));
        assertEquals(seeds[0] + seeds[1], calculator.visit((AddCalculator)root));

        root = new DivCalculator(new LeafCalculator(seeds[0]), new LeafCalculator(seeds[1]));
        assertEquals(seeds[0] / seeds[1], calculator.visit((DivCalculator)root));

        root = new MultCalculator(new LeafCalculator(seeds[0]), new LeafCalculator(seeds[1]));
        assertEquals(seeds[0] * seeds[1], calculator.visit((MultCalculator)root));

        root = new SubCalculator(new LeafCalculator(seeds[0]), new LeafCalculator(seeds[1]));
        assertEquals(seeds[0] - seeds[1], calculator.visit((SubCalculator)root));
    }

    @Test
    @Grade
    @GradeFeedback(message = "Your code couldn't handle a more complex expression", onFail = true)
    public void testTrees(){

        VisitorCalculator calculator = new EvaluationCalculator();

        Integer [] seeds = Stream.generate(rng).limit(3).toArray(Integer[]::new);
        NodeCalculator root = new AddCalculator(new MultCalculator(new LeafCalculator(seeds[0]), new LeafCalculator(seeds[1])), new LeafCalculator(seeds[2]));

        int expression = calculator.visit((AddCalculator)root);

        assertEquals(expression, (seeds[0] * seeds[1]) + seeds[2]);

        seeds = Stream.generate(rng).limit(6).toArray(Integer[]::new);
        root = new MultCalculator( new AddCalculator(
                new MultCalculator( new LeafCalculator(seeds[0]), new LeafCalculator(seeds[1])),
                new LeafCalculator(seeds[2]) ),
                new SubCalculator(
                        new DivCalculator( new LeafCalculator(seeds[3]), new LeafCalculator(seeds[4]) ),
                        new LeafCalculator(seeds[5]) ) );

        expression = calculator.visit((MultCalculator)root);
        assertEquals(expression, ( (seeds[0] * seeds[1]) + seeds[2] ) * ( (seeds[3] / seeds[4]) - seeds[5] ) );

    }


    @Test
    @Grade
    @GradeFeedback(message = "You should not be able to divide by 0, read the assignment about what we expect from your code in that specific case", onFail = true)
    public void testDivByZero(){

        VisitorCalculator calculator = new EvaluationCalculator();

        Integer rand = rng.get();
        NodeCalculator root = new DivCalculator(new LeafCalculator(rand), new LeafCalculator(0));

        try{
            int expression = calculator.visit((DivCalculator)root);
        } catch (IllegalArgumentException e){
            // this is what we expect.
        } catch (Exception e){
            fail();
        }
    }


}
