public class ExerciseTestBinarySearch {
    /*
     * public static int binarySearch(int [] arr, int low, int high, int elem) {}
     *
     * This method returns:
     *             index of elem if it is between the "low" and "high" indexes.
     *             -1 if elem is not the there
     *             -2 if the parameters do not respect the preconditions.
     *
     * @pre low >= 0, high < |arr|, low <= high
     * @post returns :
     *            index in which the searched element is located,
     *            -1 if it is not present.
     *            -2 if it does not respect @pre
     */
    public static int binarySearch(int [] arr, int low, int high, int elem) {
        if (low < 0 || high >= arr.length || low > high) { return -2; }
        while (low <= high) {
            int mid = low + (high - low) / 2;
            int midVal = arr[mid];
            if (midVal < elem)
                low = mid + 1;
            else if (midVal > elem)
                high = mid - 1;
            else
                return mid; // key found
        }
        return -1; // key not found.
    }
}
