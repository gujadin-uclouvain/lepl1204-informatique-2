import org.junit.Test;
import static org.junit.Assert.*;

public class TestSuiteBinarySearch {

    @Test
    public void testUnrespectPre1(){
        int [] array = new int[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        int result = ExerciseTestBinarySearch.binarySearch(array, 5, 6, 0);
        assertEquals(-1, result);
    }

    @Test
    public void testUnrespectPre2(){
        int [] array = new int[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        int result = ExerciseTestBinarySearch.binarySearch(array, -1, array.length-1, 0);
        assertEquals(-2, result);
    }

    @Test
    public void testUnrespectPre3(){
        int [] array = new int[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        int result = ExerciseTestBinarySearch.binarySearch(array, 0, array.length, 0);
        assertEquals(-2, result);
    }

    @Test
    public void testUnexist1(){
        int [] array = new int[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        int result = ExerciseTestBinarySearch.binarySearch(array, 0, array.length-1, 42);
        assertEquals(-1, result);
    }

    @Test
    public void testExistFirst(){
        int [] array = new int[] {2, 3, 4, 5, 6, 7, 8, 9};
        int result = ExerciseTestBinarySearch.binarySearch(array, 0, array.length-1, 2);
        assertEquals(0, result);
    }

    @Test
    public void testExistLast(){
        int [] array = new int[] {2, 3, 4, 5, 6, 7, 8, 9};
        int result = ExerciseTestBinarySearch.binarySearch(array, 0, array.length-1, 9);
        assertEquals(7, result);
    }

    @Test
    public void testExistMiddle(){
        int [] array = new int[] {2, 3, 4, 6, 7, 8, 9};
        int result = ExerciseTestBinarySearch.binarySearch(array, 0, array.length-1, 6);
        assertEquals(3, result);
    }

    @Test
    public void testExist1(){
        int [] array = new int[] {8, 12, 42, 58, 68, 78, 95, 150};
        int result = ExerciseTestBinarySearch.binarySearch(array, 0, array.length-1, 42);
        assertEquals(2, result);
    }

    @Test
    public void testExist2(){
        int [] array = new int[] {8, 12, 42, 58, 68, 78, 95, 150};
        int result = ExerciseTestBinarySearch.binarySearch(array, 0, array.length-1, 78);
        assertEquals(5, result);
    }

}
