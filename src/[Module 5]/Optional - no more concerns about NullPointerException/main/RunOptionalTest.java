import java.util.Optional;

public class RunOptionalTest {
    public static void main(String[] args) {
        OptionalTest optionalTest = new OptionalTest();
        TeamLeader teamLeader = null; // can be null
        Optional<TeamLeader> tl = optionalTest.createOptionalTeamLeader(teamLeader);
        System.out.println(tl.equals(Optional.empty())); //should not throw a NullPointerException
    }
}
