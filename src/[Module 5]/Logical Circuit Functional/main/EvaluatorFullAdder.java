import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;

public class EvaluatorFullAdder {

    public BiFunction<Boolean, Boolean, Boolean> xor_gate() { return (aBoolean, bBoolean) -> aBoolean ^ bBoolean; }

    public BiFunction<Boolean, Boolean, Boolean> or_gate() {
        return (aBoolean, bBoolean) -> aBoolean || bBoolean;
    }

    public BiFunction<Boolean, Boolean, Boolean> and_gate() {
        return (aBoolean, bBoolean) -> aBoolean && bBoolean;
    }

    public Function<Boolean, Boolean> not_gate() {
        return (Boolean) -> !Boolean;
    }

    // Should return a map with the results stored in map ( use HashMap )
    // Keys are "SUM", "CarryOut"
    // WARNING : USE HERE ONLY the previously defined method to compute result (as inginious will prevent you to cheat to directly invoke logical operators)
    public Map<String, Boolean> evaluate_circuit(Boolean a, Boolean b, Boolean carry_in) {
        HashMap<String, Boolean> output = new HashMap<>();

        BiFunction<Boolean, Boolean, Boolean> xor = xor_gate();
        BiFunction<Boolean, Boolean, Boolean> or = or_gate();
        BiFunction<Boolean, Boolean, Boolean> and = and_gate();
        Function<Boolean, Boolean> not =  not_gate();

        boolean ab_XOR = xor.apply(a, b);
        boolean sum = xor.apply(ab_XOR, carry_in);
        boolean carry_out = or.apply( and.apply(ab_XOR, carry_in), and.apply(a, b));

        output.put("SUM", sum);
        output.put("CarryOut", carry_out);

        return output;
    }

}