public class ConsMFC {
    // the item inside this list node
    public int v;

    // The next element, null if nothing
    public ConsMFC next;

    // creates a new Cons that applies function f on all elements
    public ConsMFC map(F f) {
        return new ConsMFC( f.apply(v) , next == null ? null : next.map(f) );
    }

    // creates a new Cons with all elements that matches predicate p
    public ConsMFC filter(P p) {
        return p.filter(v) ?  new ConsMFC( v , next == null ? null : next.filter(p))  :  next == null ? null : next.filter(p);
    }

    public ConsMFC filterLong(P p) {
        if (p.filter(v)) {
            return new ConsMFC( v , next == null ? null : next.filter(p));
        } else {
            return next == null ? null : next.filter(p);
        }
    }

    public ConsMFC filterVeryLong(P p) {
        if (p.filter(v)) {
            ConsMFC temp;

            if (next != null) {
                temp = next.filter(p);
            } else {
                temp = null;
            }
            return new ConsMFC( v , temp);

        } else {
            if (next != null) {
                return next.filter(p);
            } else {
                return null;
            }
        }
    }

    // Constructor
    public ConsMFC(int v, ConsMFC next) {
        this.v = v;
        this.next = next;
    }
}