import java.util.Random;

public class RunnerFListWithMergeSort {

    private static Random rng = new Random();
    public static int randomInt(){
        return rng.nextInt(100);
    }

    public static void main(String[] args) {
        FListWithMergeSort<Integer> fList = FListWithMergeSort.nil();
        for (int i = 0; i < 20; i++) {
            int r = randomInt();
            fList = fList.cons(r);
        }

        System.out.println(fList);
        FListWithMergeSort<Integer> result = fList.mergeSort();
        System.out.println(result);

    }
}
