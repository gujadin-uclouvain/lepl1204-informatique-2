import java.util.Iterator;

/* Abstract Class FList */
public abstract class FListWithMergeSort<A> implements Iterable<A>{

    public final boolean isNotEmpty() { return this instanceof Cons; }

    public final boolean isEmpty() { return this instanceof Nil; }

    public abstract int length();

    public abstract A head();

    public abstract FListWithMergeSort<A> tail();

    public static <A> FListWithMergeSort<A> nil() { return (Nil<A>) Nil.INSTANCE; }

    public final FListWithMergeSort<A> cons(final A a) { return new Cons(a, this); }

    public FListWithMergeSort<Integer> mergeSort() {
        FListWithMergeSort<Integer> fList = (FListWithMergeSort<Integer>) this;
        return FListMerge.mergeSort(fList);
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append("(");
        int length = this.length();
        for (A value: this) {
            result.append(value.toString()); length--;
            if (length > 0) { result.append(", "); }
        }
        result.append(")");
        return result.toString();
    }

    /* ITERATOR */
    public Iterator<A> iterator() {
        FListWithMergeSort<A> flistRef = this;
        return new Iterator<A>() {
            private FListWithMergeSort<A> current = flistRef;

            public boolean hasNext() {
                return !current.isEmpty();
            }

            public A next() {
                if (hasNext()) {
                    A value = current.head();
                    current = current.tail();
                    return value;
                }
                throw new java.util.NoSuchElementException();
            }

            public void remove() {
                throw new UnsupportedOperationException();
            }
        };
    }

    /* Class Nil */
    public static final class Nil<A> extends FListWithMergeSort<A> {
        private static  final Nil<Object> INSTANCE = new Nil();

        @Override
        public int length() {
            return 0;
        }

        @Override
        public A head() {
            return null;
        }

        @Override
        public FListWithMergeSort<A> tail() {
            return FListWithMergeSort.nil();
        }
    }

    /* Class Cons */
    public static final class Cons<A> extends FListWithMergeSort<A> {

        private A head;
        private FListWithMergeSort<A> tail;
        private int length;

        public Cons(A a, FListWithMergeSort<A> as) {
            this.head = a;
            this.tail = as;
            this.length = this.tail().length() + 1;
        }

        @Override
        public int length() {
            return this.length;
        }

        @Override
        public A head() {
            if (isEmpty()) throw new java.util.NoSuchElementException();
            return head;
        }

        @Override
        public FListWithMergeSort<A> tail() {
            if (isEmpty()) throw new java.util.NoSuchElementException();
            return tail;
        }
    }

    public static class FListMerge {
        private static FListWithMergeSort<Integer> merge(FListWithMergeSort<Integer> valueA, FListWithMergeSort<Integer> valueB) {
            FListWithMergeSort<Integer> result = FListWithMergeSort.nil();
            while (valueA.isNotEmpty() && valueB.isNotEmpty()) {
                if (valueA.head() <= valueB.head()) {
                    result = result.cons(valueA.head());
                    valueA = valueA.tail();
                } else {
                    result = result.cons(valueB.head());
                    valueB = valueB.tail();
                }
            }

            FListWithMergeSort<Integer> restToAdd = valueA.isNotEmpty() ? valueA : valueB;
            while (restToAdd.isNotEmpty()) {
                result = result.cons(restToAdd.head());
                restToAdd = restToAdd.tail();
            }

            return result;
        }

        public static FListWithMergeSort<Integer> mergeSort(FListWithMergeSort<Integer> fList) {
            if (fList.length() == 1) { return fList; }

            FListWithMergeSort<Integer> left = FListWithMergeSort.nil();
            FListWithMergeSort<Integer> right = FListWithMergeSort.nil();
            int middle = fList.length() / 2;
            int increment = 0;
            for (Integer value : fList) {
                if ((increment < middle)) { left = left.cons(value); }
                else                      { right = right.cons(value); }
                increment++;
            }

            left = mergeSort(left);
            right = mergeSort(right);

            FListWithMergeSort<Integer> mergeResult = merge(left, right);

            FListWithMergeSort<Integer> mergeInversed = FListWithMergeSort.nil();
            for (Integer integer : mergeResult) { mergeInversed = mergeInversed.cons(integer); }

            return mergeInversed;
        }
    }
}

