import java.util.Comparator;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class StudentFunctions2 implements StudentStreamFunction2 {

  // In order to test efficiently your code, we use a Map<String, Predicate<?>> structured like that :
  //    Key : String that is one of the fields of Student ( "firstName", "lastName", "section", "courses_results")
  //    Value : Predicate bounded to the type of the field to perform a check condition
private Stream<Student2> filterConditions(Stream<Student2> studentsStream, Map<String, Predicate<?>> conditions){
  for (Map.Entry<String, Predicate<?>> condition : conditions.entrySet()) {
      String key = condition.getKey();
      if (key.equals("firstName")) {
        Predicate<String> checkF = (Predicate<String>) condition.getValue();
        studentsStream = studentsStream.filter(student -> checkF.test(student.getFirstName()));
      } else if (key.equals("lastName")) {
        Predicate<String> checkL = (Predicate<String>) condition.getValue();
        studentsStream = studentsStream.filter(student -> checkL.test(student.getLastName()));
      } else if (key.equals("section")) {
        Predicate<Integer> checkS = (Predicate<Integer>) condition.getValue();
        studentsStream = studentsStream.filter(student -> checkS.test(student.getSection()));
      } else if (key.equals("courses_results")) {
        Predicate<Map<String, Double>> checkR = (Predicate<Map<String, Double>>) condition.getValue();
        studentsStream = studentsStream.filter(student -> checkR.test(student.getCourses_results()));
      }
  }
  return studentsStream;
}

  // Returns a student if any match the given conditions
  // if it is not possible, you must return null
  public Student2 findFirst(Stream<Student2> studentsStream, Map<String, Predicate<?>> conditions){
      return filterConditions(studentsStream, conditions).findFirst().orElse(null);
  }

  // Returns a array of student(s) that match the given conditions
  public Student2[] findAll(Stream<Student2> studentsStream, Map<String, Predicate<?>> conditions){
      return filterConditions(studentsStream, conditions).toArray(Student2[]::new);
  }

  // Return true if we could find n student(s) that match the given condition
  public boolean exists(Stream<Student2> studentsStream, Map<String, Predicate<?>> conditions, int n){
      return filterConditions(studentsStream, conditions).count() >= n;
  }

  // Returns a ordered array of student(s) that match the given conditions,
  // depending of the given comparator
  public Student2[] filterThenSort(Stream<Student2> studentsStream, Map<String, Predicate<?>> conditions, Comparator<Student2> comparator) {
      return filterConditions(studentsStream, conditions).sorted(comparator).toArray(Student2[]::new);
  }
}
