import java.util.Comparator;
import java.util.function.Function;
import java.util.function.Predicate;

public class LambdaExamples {
    public static void main(String[] args) {
        LambdaExamples lambda = new LambdaExamples();

        Function<Integer, Integer> f1 = (i) -> i+2;
        System.out.println(f1.apply(2));

        Function<String, Integer> mf = String::length; // (str) -> str.length();
        System.out.println(mf.apply("Hello"));

        Predicate<Integer> p1 = (i) -> i/2 == i-(i/2);
        System.out.println(p1.test(9));

        Comparator<String> comp = String::compareTo; // (o1, o2) -> o1.compareTo(o2);
        System.out.println(comp.compare("Test", "Test"));
        System.out.println(comp.compare("Test", "Tests"));

        Comparator<String> compLength = Comparator.comparingInt(String::length); // (o1, o2) -> o1.length() - o2.length();
        System.out.println(compLength.compare("Test", "Test"));
        System.out.println(compLength.compare("Test", "Tests"));
        System.out.println(compLength.compare("Tests", "Test"));
    }
}
