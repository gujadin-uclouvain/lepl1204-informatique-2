import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public class Student1 implements Comparable<Student1> {
    private String firstName;
    private String lastName;
    private int section;
    private Map<String, Double> coursesResults = new HashMap<>();

    public Student1(String firstName, String lastName, int section, Map<String, Double> coursesResults) {
        this.coursesResults = coursesResults;
        this.firstName = firstName;
        this.lastName = lastName;
        this.section = section;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getSection() {
        return section;
    }

    public Map<String, Double> getCoursesResults() {
        return coursesResults;
    }

    @Override
    public int compareTo(Student1 student) {
        // Advanced sorting
        Comparator<Student1> myComparator = Comparator
                .comparing(Student1::getSection)
                .thenComparing(Student1::getFirstName)
                .thenComparing(Student1::getLastName)
                .thenComparing(
                        (s) -> s.getCoursesResults().values().stream().reduce(0.0, Double::sum),
                        Comparator.reverseOrder()
                );
        return myComparator.compare(this, student);
    }

    @Override
    public boolean equals(Object obj) {

        if (obj == null || !(obj instanceof Student1)) {
            return false;
        }

        return this.compareTo((Student1) obj) == 0;
    }
}
