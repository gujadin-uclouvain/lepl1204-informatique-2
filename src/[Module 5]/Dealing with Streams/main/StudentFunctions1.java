import java.util.Comparator;
import java.util.stream.Stream;

public class StudentFunctions1 implements StudentStreamFunction1 {

  // Find the N°2 and N°3 top students for the given course name in parameter
  public Stream<Student1> findSecondAndThirdTopStudentForGivenCourse(Stream<Student1> studentStream, String name){
    return studentStream.filter( student -> student.getCoursesResults().containsKey(name) )
                        .sorted( (stud1, stud2) -> Double.compare(0.0, stud1.getCoursesResults().get(name) - stud2.getCoursesResults().get(name)) )
                        .skip(1)
                        .limit(2);
  }

  // Compute for each student in the given section their average grade result,
  // sorted by their result (ascending) as an array of array structured like that :
  // [ [ "Student FirstName1 LastName1", 7.5 ], [ "Student FirstName2 LastName2", 9.5 ]  ]
  public Object[] computeAverageForStudentInSection(Stream<Student1> studentStream, int section){
    return studentStream.filter( student -> student.getSection() == section )
                        .map( student -> new Object[] { "Student " + student.getFirstName() + " " + student.getLastName(),
                              student.getCoursesResults().values().stream().reduce(Double::sum).orElse(0.0) / student.getCoursesResults().size() } )
                        .toArray();
  }

  // Give the number of students that success in all courses (> 10.0)
  public int getNumberOfSuccessfulStudents(Stream<Student1> studentStream){
    return (int) studentStream.filter( student -> student.getCoursesResults().values().stream().allMatch( res -> res > 10.0) ).count();
  }

  // Find the student that is the last one in the lexicographic order
  // ( You must first compare students on their lastNames then on their firstNames )
  public Student1 findLastInLexicographicOrder(Stream<Student1> studentStream){
    return studentStream.max(Comparator.comparing(Student1::getLastName).thenComparing(Student1::getFirstName) ).orElse(null);
  }

  // Give the full sum of the grade obtained by all students
  public double getFullSum(Stream<Student1> studentStream){
    Stream<Double> gradePerStudent = studentStream.map(student -> student.getCoursesResults().values().stream().reduce(0.0, Double::sum));
    return gradePerStudent.reduce( 0.0, Double::sum);
  }
}
