import java.util.Map;
import java.util.stream.Stream;

public class StudentRun1 {
    public static void main(String[] args) {
        Stream<Student1> studentStream = Stream.of( new Student1("Dominique", "Jadin", 1, Map.of("test", 1.0)),
                                                   new Student1("Alexandre", "Jadin", 1, Map.of("test", 1.0)),
                                                   new Student1("Guillaume", "Jadin", 1, Map.of("test", 1.0)));
        StudentFunctions1 f = new StudentFunctions1();
        Student1 last = f.findLastInLexicographicOrder(studentStream);
        System.out.println(last.getFirstName());
    }
}
