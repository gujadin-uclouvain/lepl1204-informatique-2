import java.util.function.Function;

public abstract class FTreeImmutable<A> {

    public abstract A value();
    public abstract FTreeImmutable<A> left();
    public abstract FTreeImmutable<A> right();

    public final int depth() {
        return left() instanceof LeafFTreeImmutable ? 1 : 1 + left().depth();
    }

    public final <B> FTreeImmutable<B> map(Function<A,B> f) {
        if (this instanceof LeafFTreeImmutable) {
            return new LeafFTreeImmutable<B>( f.apply(this.value()) );
        }
        return new NodeFTreeImmutable<B>( f.apply(this.value()), this.left().map(f), this.right().map(f) );
    }
}
