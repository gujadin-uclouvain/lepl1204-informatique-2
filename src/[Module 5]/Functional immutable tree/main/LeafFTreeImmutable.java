public class LeafFTreeImmutable<A> extends FTreeImmutable<A> {

    private final A value;

    public LeafFTreeImmutable(A a) { this.value = a; }

    @Override
    public A value() { return this.value; }

    @Override
    public FTreeImmutable<A> left() { return null; }

    @Override
    public FTreeImmutable<A> right() { return null; }
}
