public class NodeFTreeImmutable<A> extends FTreeImmutable<A> {

    private final A value;
    private final FTreeImmutable<A> left, right;

    public NodeFTreeImmutable(final A a, FTreeImmutable<A> left, FTreeImmutable<A> right){
        this.value = a;
        this.left = left;
        this.right = right;
    }

    @Override
    public A value() { return this.value; }

    @Override
    public FTreeImmutable<A> left() { return this.left; }

    @Override
    public FTreeImmutable<A> right() { return this.right; }
}
