import com.github.guillaumederval.javagrading.CustomGradingResult;
import com.github.guillaumederval.javagrading.Grade;
import com.github.guillaumederval.javagrading.GradeFeedback;
import com.github.guillaumederval.javagrading.GradingRunner;
import com.github.guillaumederval.javagrading.TestStatus;
import org.junit.Test;
import org.junit.runner.RunWith;


import java.util.Random;

import static org.junit.Assert.*;


@RunWith(GradingRunner.class) // classic "jail runner" from Guillaume's library
public class TestsFTreeImmutable {

    private static Random rng = new Random();

    public static int randomInt(){
        return rng.nextInt(100);
    }

    public static int fTreeSum(FTreeImmutable<Integer> ft){
        if(ft instanceof NodeFTreeImmutable){
            return ft.value() + fTreeSum(ft.left()) + fTreeSum(ft.right());
        }
        return ft.value();
    }

    @Test()
    @Grade(value=5, custom=true, cpuTimeout=100)
    @GradeFeedback(message = "Your map function doesn't give the expected output", onFail = true, onTimeout = true)
    public void testSimpleMap() throws CustomGradingResult{

        FTreeImmutable<Integer> root;
        FTreeImmutable[] firstStage = new FTreeImmutable[2];
        FTreeImmutable[] secondStage = new FTreeImmutable[4];
        FTreeImmutable[] thirdStage = new FTreeImmutable[8];
        FTreeImmutable[] fleafs = new FTreeImmutable[16];

        for(int i = 0 ; i < 16 ; i++){
            fleafs[i] = new LeafFTreeImmutable(1+(2*i));
        }

        for(int i = 0; i<8 ; i++){
            thirdStage[i] = new NodeFTreeImmutable<Integer>(2 + i*4, fleafs[i*2], fleafs[i*2+1]);
        }

        for(int i = 0 ; i <4 ; i++){
            secondStage[i] = new NodeFTreeImmutable<Integer>(((i*8)+4), thirdStage[i*2], thirdStage[i*2+1]);
        }

        firstStage[0] = new NodeFTreeImmutable<Integer>(8, secondStage[0], secondStage[1]);
        firstStage[1] = new NodeFTreeImmutable<Integer>(24, secondStage[2], secondStage[3]);

        root = new NodeFTreeImmutable<Integer>(16, firstStage[0], firstStage[1]);

        FTreeImmutable<Integer> result = root.map(i -> i*10);

        assertEquals(4960, fTreeSum(result) );



    }

    @Test()
    @Grade(value=5, custom=true, cpuTimeout=100)
    @GradeFeedback(message = "Your map function doesn't give the expected output", onFail = true, onTimeout = true)
    public void testRandomMap() throws CustomGradingResult{

        int elemSum = 0 ;
        int r;

        FTreeImmutable<Integer> root;
        FTreeImmutable[] firstStage = new FTreeImmutable[2];
        FTreeImmutable[] secondStage = new FTreeImmutable[4];
        FTreeImmutable[] thirdStage = new FTreeImmutable[8];
        FTreeImmutable[] fleafs = new FTreeImmutable[16];
        for(int i = 0 ; i < 16 ; i++){
            r = randomInt();
            elemSum += r;
            fleafs[i] = new LeafFTreeImmutable(r);
        }

        for(int i = 0; i<8 ; i++){
            r = randomInt();
            elemSum += r;
            thirdStage[i] = new NodeFTreeImmutable<Integer>(r, fleafs[i*2], fleafs[i*2+1]);
        }

        for(int i = 0 ; i <4 ; i++){
            r = randomInt();
            elemSum += r;
            secondStage[i] = new NodeFTreeImmutable<Integer>(r, thirdStage[i*2], thirdStage[i*2+1]);
        }

        r = randomInt();
        elemSum += r;
        firstStage[0] = new NodeFTreeImmutable<Integer>(r, secondStage[0], secondStage[1]);

        r = randomInt();
        elemSum += r;
        firstStage[1] = new NodeFTreeImmutable<Integer>(r, secondStage[2], secondStage[3]);

        r = randomInt();
        elemSum += r;
        root = new NodeFTreeImmutable<Integer>(r, firstStage[0], firstStage[1]);

        FTreeImmutable<Integer> result = root.map(i -> i*10);

        assertEquals(elemSum*10, fTreeSum(result) );


    }

    @Test()
    @Grade(value=5, custom=true, cpuTimeout=100)
    @GradeFeedback(message = "Your depth function doesn't give the expected output", onFail = true, onTimeout = true)
    public void testDepth() throws CustomGradingResult{


        FTreeImmutable<Integer> root;
        FTreeImmutable[] firstStage = new FTreeImmutable[2];
        FTreeImmutable[] secondStage = new FTreeImmutable[4];
        FTreeImmutable[] thirdStage = new FTreeImmutable[8];
        FTreeImmutable[] fleafs = new FTreeImmutable[16];
        for(int i = 0 ; i < 16 ; i++){
            fleafs[i] = new LeafFTreeImmutable(1);
        }

        for(int i = 0; i<8 ; i++){
            thirdStage[i] = new NodeFTreeImmutable<Integer>(1, fleafs[i*2], fleafs[i*2+1]);
        }

        for(int i = 0 ; i <4 ; i++){
            secondStage[i] = new NodeFTreeImmutable<Integer>(1, thirdStage[i*2], thirdStage[i*2+1]);
        }

        firstStage[0] = new NodeFTreeImmutable<Integer>(1, secondStage[0], secondStage[1]);
        firstStage[1] = new NodeFTreeImmutable<Integer>(1, secondStage[2], secondStage[3]);

        root = new NodeFTreeImmutable<Integer>(1, firstStage[0], firstStage[1]);
        int test = root.depth();
        assertEquals(4, root.depth());


    }

    @Test()
    @Grade(value=5, custom=true, cpuTimeout=100)
    @GradeFeedback(message = "Your Leaf class is not correctly implemented", onFail = true, onTimeout = true)
    public void testLeaf() throws CustomGradingResult{

        FTreeImmutable<Integer> l = new LeafFTreeImmutable(1);

        if(null!=l.left()) throw new CustomGradingResult(TestStatus.FAILED, 0, "left() on a leaf should return null");

        if(null!=l.right()) throw new CustomGradingResult(TestStatus.FAILED, 0, "right() on a leaf should return null");

        if(1!=l.value()) throw new CustomGradingResult(TestStatus.FAILED, 0, "The value returned by value() is not the one given to the constructor");

        assertEquals(null, l.left());
        assertEquals(null, l.right());


    }

    @Test()
    @Grade(value=5, custom=true, cpuTimeout=100)
    @GradeFeedback(message = "Your Node class is not correctly implemented", onFail = true, onTimeout = true)
    public void testNode() throws CustomGradingResult{

        FTreeImmutable<Integer> l = new LeafFTreeImmutable(1);
        FTreeImmutable<Integer> r = new LeafFTreeImmutable(3);
        FTreeImmutable<Integer> n = new NodeFTreeImmutable(2, l, r);

        if(l!=n.left()) throw new CustomGradingResult(TestStatus.FAILED, 0, "left() on a leaf should return the child");

        if(r!=n.right()) throw new CustomGradingResult(TestStatus.FAILED, 0, "right() on a leaf should return the child");

        if(2!=n.value()) throw new CustomGradingResult(TestStatus.FAILED, 0, "The value returned by value() is not the one given to the constructor");

        assertEquals(l, n.left());
        assertEquals(r, n.right());


    }

}
