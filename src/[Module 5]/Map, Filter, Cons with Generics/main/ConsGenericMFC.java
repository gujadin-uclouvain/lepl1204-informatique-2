import java.util.function.Predicate;
import java.util.function.Function;

public class ConsGenericMFC<E> {
    // the item inside this list node
    public E v;

    // The next element, null if nothing
    public ConsGenericMFC<E> next;

    // Constructor
    public ConsGenericMFC(E v, ConsGenericMFC<E> next) {
        this.v = v;
        this.next = next;
    }

    public <R> ConsGenericMFC<R> map(Function <E,R> function) {
      return new ConsGenericMFC<R>(function.apply(v), next == null ? null : next.map(function));
    }

    public ConsGenericMFC<E> filter(Predicate <E> predicate) {
      return predicate.test(v) ? new ConsGenericMFC<E>( v , next == null ? null : next.filter(predicate)) : next == null ? null : next.filter(predicate);
    } 
}
