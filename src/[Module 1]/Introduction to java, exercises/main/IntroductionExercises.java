import java.util.Arrays;

public class IntroductionExercises {
    public static int variable = 0;
    public static int[] squares;

    public static void attribute(int value) {
        variable = value;
    }

    public static int add(int a, int b) {
        return a + b;
    }

    public static boolean equalsIntegers(int a, int b) {
        return (a == b);
    }

    public static int middleValue(int a, int b, int c) {
        if ((a == b) || (a == c) || (b == c)) {
            return -1;
        } else if (a > b) {
            if (b > c) {
                return b;
            } else {
                if (a > c) {
                    return c;
                } else {
                    return a;
                }
            }
        } else {
            if (a > c) {
                return a;
            } else {
                if (b > c) {
                    return c;
                } else {
                    return b;
                }
            }
        }
    }

    public static int middleValueUP(int a, int b, int c) {
        if ((a == b) || (a == c) || (b == c)) return -1;
        return Arrays.stream(new int[]{a, b, c}).sorted().skip(1).findFirst().orElseThrow();
    }

    public static int max(int a, int b) { return Math.max(a, b); }

    public static String greetings(String str) {
        switch (str) {
            case "Morning":
                return "Good morning, sir!";
            case "Evening":
                return "Good evening, sir!";
            default:
                return "Hello, sir!";
        }
    }

    public static String greetingsUP(String str) {
        return str.equals("Morning") ? "Good morning, sir!" : (str.equals("Evening") ? "Good evening, sir!" : "Hello, sir!");
    }

    public static int[] lastFirstMiddle(int[] a) {
        int[] new_array;
        new_array = new int[]{a[a.length - 1], a[0], a[a.length / 2]};
        return new_array;
    }

    public static int[] lastFirstMiddleUP(int[] a) { return new int[]{a[a.length-1], a[0], a[a.length/2]}; }

    public static int sum(int[] array) {
        int result = 0;
        for (int elem : array) {
            result += elem;
        }
        return result;
    }

    public static int sumUP(int[] array) { return Arrays.stream(array).sum(); }

    public static int maxArray(int[] array) {
        int max = array[0];
        int index = 1;
        while (index < array.length) {
            if (array[index] > max) {
                max = array[index];
            }
            index++;
        }
        return max;
    }

    public static int maxArrayUP(int[] array) { return Arrays.stream(array).max().orElse(-1); }

    public static void main(String... args) {
        squares = new int[args.length];
        for (int index = 0; index < args.length; index++) {
            try {
                int temp = Integer.parseInt(args[index]);
                squares[index] = (int) Math.pow(temp,2);
            } catch (NumberFormatException e) {
                squares[index] = 0;
            }
        }
        System.out.println(Arrays.toString(squares));
    }
}