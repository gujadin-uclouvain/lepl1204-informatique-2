import java.util.ArrayList;

public class Decoder {

    /*
     * Forbidden characters are passed as an array of int.
     * Each element of this array correspond to the decimal ASCII code
     * of a forbidden character OR null if there's no forbidden character
     * If you encounter one of these forbidden character
     * you must ignore it when you translate your sentence.
     *
     * the 2D array "sentences" contain a set of decimal ASCII code we want you
     * to translate. Each sub-element of this array is a different sentence.
     * Ex : if we pass this array : [ ["42", "72", "88"], ["98", "99", "111", "47", "55"]]
     * to your decode method, you should return : [ "*HX", "bco/7" ]
     *
     * You should NEVER return null or an array containing null
     */
    public static String [] decode(int[] forbidden, String[][] sentences){
        ArrayList<String> listASCII = new ArrayList<>();
        for (String[] sentence : sentences) {
            StringBuilder strPerList = new StringBuilder();
            for (String value : sentence) {
                boolean passAdding = false;
                int intValue = Integer.parseInt(value);
                if (forbidden != null){
                    int indexForbidden = 0;
                    while (indexForbidden < forbidden.length && !passAdding){
                        if (intValue == forbidden[indexForbidden]){
                            passAdding = true;
                        }
                        indexForbidden++;
                    }
                }
                if (!passAdding){
                    strPerList.append((char) intValue);
                }
            }
            listASCII.add(strPerList.toString());
        }
        return listASCII.toArray(String[]::new);
    }

}