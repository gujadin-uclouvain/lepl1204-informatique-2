import java.util.Arrays;

public class RunDecoder {
    public static void main(String[] args) {
        String[][] array2d = new String[][]{{"084", "104", "105", "115", "032", "105", "115", "032"},
                {"097", "032", "108", "097", "114", "103", "101", "114"},
                {"115", "101", "116", "032", "111", "102", "032", "115", "101", "110",
                        "116", "101", "110", "099", "101", "115", "046", "046", "046"}};
        int[] forgotten = new int[]{32, 101};
        System.out.println(Arrays.toString(Decoder.decode(forgotten, array2d)));
    }
}
