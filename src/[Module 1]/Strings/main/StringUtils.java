public class StringUtils {

    /*
     * Split the input string 'str' w.r.t the character 'marker' in an array of String
     * for example split("test-test", '-') => {"test", "test"}
     * Must return null if there is no occurrence of 'marker' in 'str'
     */
    public static String[] split(String str, char marker) {
        java.util.ArrayList<String> splitArrayList = new java.util.ArrayList<>();
        int indexStart = 0;
        for (int index = 0; index < str.length(); index++) {
            if (str.charAt(index) == marker) {
                String temp = str.substring(indexStart, index);
                if (temp.length() != 0) {
                    splitArrayList.add(temp);
                }
                indexStart = index + 1;
            }
        }
        String temp = str.substring(indexStart);
        if (temp.length() != 0) {
            splitArrayList.add(temp);
        }
        if (temp.equals(str)) {
            return null;
        }
        String[] splitArray = new String[splitArrayList.size()];
        return splitArrayList.toArray(splitArray);
    }

    /*
     * Returns the index of the first occurrence of sub in str
     * or -1 if there is no occurrence of sub in str at all.
     * Be careful, we ask you to make CASE SENSITIVE comparison between str and sub.
     */
    public static int indexOf(String str, String sub) {
        for (int index = 0; index <= str.length() - sub.length(); index++) {
            int indexFirst = index;
            int indexSub = 0;
            while (indexSub < sub.length() && str.charAt(indexFirst) == sub.charAt(indexSub)) {
                if (indexSub == sub.length() - 1) {
                    return index;
                }
                indexSub++;
                indexFirst++;
            }
        }
        return -1;
    }

    /*
     * Returns a String with the same characters as 'str' except that
     * all upper case characters have been replaced by their lower case equivalent.
     */
    public static String toLowerCase(String str) {
        StringBuilder strFinal = new StringBuilder();
        for (int index = 0; index < str.length(); index++) {
            char currentChar = str.charAt(index);
            if (Character.isUpperCase(currentChar)) {
                int codePointChar = currentChar + 32;
                strFinal.append((char) codePointChar);
            } else {
                strFinal.append(currentChar);
            }
        }
        return strFinal.toString();
    }

    /*
     * Returns true if the string 'str' is a palindrome (a string that reads the same from
     * left to right AND from right to left).
     */
    public static boolean palindrome(String str) {
        int offset = 0;
        while (str.charAt(offset) == str.charAt(str.length() - 1 - offset)) {
            if (offset >= (str.length() - 1) / 2) {
                return true;
            }
            offset++;
        }
        return false;
    }
}