import java.util.Comparator;

public class PersonComparator implements Comparator<PersonComp> {
    @Override
    public int compare(PersonComp o1, PersonComp o2) {
        if (o1.name.compareTo(o2.name) == 0) {
            return o1.age - o2.age;
        }
        return o1.name.compareTo(o2.name);
    }
}
