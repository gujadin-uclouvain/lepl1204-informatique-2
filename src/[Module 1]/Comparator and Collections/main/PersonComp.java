import java.util.Comparator;

public class PersonComp {

    public String name;
    public int age;

    public PersonComp(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public boolean isBefore(PersonComp person) {
        if (this.name.compareTo(person.name) > 0) return true;
        else if (this.name.compareTo(person.name) == 0 && this.age > person.age) return true;
        return false;
    }

    @Override
    public String toString() {
        return name + " " + age;
    }


}
