import java.util.ArrayList;
import java.util.Comparator;
import java.util.Random;

public class RunPersonComp {

    public static void sortPerson(ArrayList<PersonComp> persons) {
        /* Selection Sort */
        for (int firstElem = 0; firstElem < persons.size() - 1; firstElem++) {
            for (int secondElem = firstElem + 1; secondElem < persons.size(); secondElem++) {
                PersonComp one = persons.get(firstElem);
                PersonComp two = persons.get(secondElem);
                if (one.isBefore(two)) {
                    persons.remove(firstElem); persons.add(firstElem, two);
                    persons.remove(secondElem); persons.add(secondElem, one);
                }
            }
        }
    }

    public static void sortPersonUP(ArrayList<PersonComp> persons) {
        /* Insertion Sort */
        for (int i = 1; i < persons.size(); i++) {
            PersonComp key = persons.get(i);
            int j = i-1;
            PersonComp var = persons.get(j);
            while (j >= 0 && !key.isBefore(var)) {
                persons.remove(j); persons.add(j + 1, var);
                j--; if (j != -1) var = persons.get(j);
            }
            persons.remove(j + 1); persons.add(j + 1, key);
        }
    }

    public static void withThreads(ArrayList<PersonComp> personsS, ArrayList<PersonComp> personsI) {
        final long[] startTimeS = new long[1];
        final long[] endTimeS = new long[1];
        Thread t = new Thread( () -> {
            startTimeS[0] = System.currentTimeMillis();
            sortPerson(personsS);
            endTimeS[0] = System.currentTimeMillis();

        });
        t.start();
        long startTimeI = System.currentTimeMillis();
        sortPersonUP(personsI);
        long endTimeI = System.currentTimeMillis();
        System.out.println("INSERTION => " + (endTimeI - startTimeI) + " milliseconds");

        try {
            t.join();
            System.out.println("SELECTION => " + (endTimeS[0] - startTimeS[0]) + " milliseconds");
        } catch (InterruptedException e) {
            throw new RuntimeException("", e);
        }
    }

    public static void withThreadsAndJavaSort(ArrayList<PersonComp> personsS, ArrayList<PersonComp> personsJ) {
        final long[] startTimeS = new long[1];
        final long[] endTimeS = new long[1];
        Thread t = new Thread( () -> {
            startTimeS[0] = System.currentTimeMillis();
            sortPersonUP(personsS);
            endTimeS[0] = System.currentTimeMillis();

        });
        t.start();
        long startTimeI = System.currentTimeMillis();
        personsJ.sort(new PersonComparator());
        long endTimeI = System.currentTimeMillis();
        System.out.println("JAVA SORT ALGORITHM => " + (endTimeI - startTimeI) + " milliseconds");

        try {
            t.join();
            System.out.println("INSERTION => " + (endTimeS[0] - startTimeS[0]) + " milliseconds");
        } catch (InterruptedException e) {
            throw new RuntimeException("", e);
        }
    }

    public static void withoutThreads(ArrayList<PersonComp> personsS, ArrayList<PersonComp> personsI) {
        long startTimeI = System.currentTimeMillis();
        sortPersonUP(personsI);
        long endTimeI = System.currentTimeMillis();
        System.out.println("INSERTION => " + (endTimeI - startTimeI) + " milliseconds");

        long startTimeS = System.currentTimeMillis();
        sortPerson(personsS);
        long endTimeS = System.currentTimeMillis();
        System.out.println("SELECTION => " + (endTimeS - startTimeS) + " milliseconds");
    }

   public static void main(String[] args) {
        Random rnd = new Random();
        ArrayList<PersonComp> personsI = new ArrayList<>();
        ArrayList<PersonComp> personsS = new ArrayList<>();
        for (int i = 0; i < 1000; i++) {
            int r = rnd.nextInt(10000);
            personsI.add(new PersonComp("Jean",r+40));
            personsI.add(new PersonComp("Marc",r+15));
            personsS.add(new PersonComp("Jean",r+40));
            personsS.add(new PersonComp("Marc",r+15));
        }
        withThreadsAndJavaSort(personsS, personsI);
    }
}
