import java.awt.*;
import java.util.ArrayList;

public class Except {

    public static void outof() {
        int[] array = new int[2];
        array[2] = 10;
    }

    public static void nullpointer() {
        int[] array = null;
        int length = array.length;
    }

    public static void concurr() {
        ArrayList<String> OSList = new ArrayList<>();
        OSList.add("Windows 7");
        OSList.add("Ubuntu");
        OSList.add("Windows 2000");
        OSList.add("MacOs");
        for (String OS : OSList){
            if (OS.startsWith("Windows")){
                OSList.remove(OS);
            }
        }
    }
}
