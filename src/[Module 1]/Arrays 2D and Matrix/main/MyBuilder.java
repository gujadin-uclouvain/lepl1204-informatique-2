import java.util.Arrays;

public class MyBuilder implements Array2DBuilderInterface {
  public int[][] build_from(String s) {
        String[] LineSplit = s.split("\\n+");
        int LineNumber = LineSplit.length;
        int[][] array2d = new int[LineNumber][];
        int CounterColumn = 0;
        for (String Column : LineSplit){
          String[] ColumnSplit = Column.trim().split("\\s+");
          int[] FinalLine = new int[ColumnSplit.length];
          int CounterLine = 0;
          for (String elem : ColumnSplit){
              int value = Integer.parseInt(elem);
              FinalLine[CounterLine] = value;
              CounterLine++;
          }
          array2d[CounterColumn] = new int[CounterLine];
          array2d[CounterColumn] = FinalLine;
          CounterColumn++;
        }
        return array2d;
  }
  public int sum(int[][] array) {
        int result = 0;
        for (int line = 0; line < array.length; line++) {
          for (int column = 0; column < array[line].length; column++){
              result += array[line][column];
          }
        }
        return result;
  }
  public int[][] transpose(int[][] matrix) {
    int line = matrix.length;
    int column = matrix[0].length;
    int[][] TMatrix = new int[column][line];
    for (int ColumnIndex = 0; ColumnIndex < column; ColumnIndex++){
        for (int LineIndex = 0; LineIndex < line; LineIndex++){
            TMatrix[ColumnIndex][LineIndex] = matrix[LineIndex][ColumnIndex];
        }
    }
    return TMatrix;
  }
  public int[][] product(int[][] matrix1, int[][] matrix2) {
    int line1 = matrix1.length;
    int column1 = matrix1[0].length;
    int column2 = matrix2[0].length;
    int[][] MMatrix = new int[line1][column2];
    for (int Line1Index = 0; Line1Index < line1; Line1Index++){
        for (int Column2Index = 0; Column2Index < column2; Column2Index++){
            for (int Column1Index = 0; Column1Index < column1; Column1Index++){
                MMatrix[Line1Index][Column2Index] += matrix1[Line1Index][Column1Index] * matrix2[Column1Index][Column2Index];
            }
        }
    }
    return MMatrix;
    }
}