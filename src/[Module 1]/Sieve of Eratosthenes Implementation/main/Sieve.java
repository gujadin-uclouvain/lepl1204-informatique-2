import java.util.BitSet;

public class Sieve{
    
    public static BitSet bits; //You should work on this BitSet
    
    public static int numberOfPrime(int n){
        bits = new BitSet(n);
        bits.set(2, n);
        for (int indexPrime = 2; indexPrime < Math.sqrt(n); indexPrime++) {
            if (bits.get(indexPrime)) {
                for (double indexCalc = (Math.pow(indexPrime, 2)); indexCalc < n; indexCalc += indexPrime) {
                    bits.set((int) indexCalc, false);
                }
            }
        }
        return bits.cardinality();
    }

    public static void main(String[] args) {
        System.out.println(numberOfPrime(120));
    }
}